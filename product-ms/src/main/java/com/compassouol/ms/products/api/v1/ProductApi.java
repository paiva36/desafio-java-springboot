package com.compassouol.ms.products.api.v1;

import java.net.URI;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.compassouol.ms.products.service.ProductService;
import com.compassouol.ms.products.service.exceptions.ProductValidationException;
import com.compassouol.ms.products.service.filter.ProductFilter;
import com.compassouol.ms.products.service.to.ProductTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

/**
 * Endpoint Products
 * @author paiva
 *
 */
@RequiredArgsConstructor
@Tag(name = "Product Endpoint")
@RestController
@RequestMapping("/products")
public class ProductApi {

	private final ProductService service;

	/**
	 * {@code POST /products } Criação de um produto.
	 * 
	 * @param productTO o produto a ser criado.
	 * @return o {@link ResponseEntity} com status {@code 201 (created) } e o body
	 *         com o novo {@link ProductTO}, ou {@code 400 ( bad request )}
	 */
	@Operation(summary = "Criação de um produto")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = "Criação de um produto.", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ProductTO.class)) }),
			@ApiResponse(responseCode = "400", description = "Requisição inválida.", content = @Content) })
	@PostMapping(produces = { "application/json", "application/xml", "application/x-yaml" }, consumes = {
			"application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<ProductTO> create(@RequestBody ProductTO productTO, UriComponentsBuilder uriBuilder) {

		if (productTO.getKey() != null) {
			throw new ProductValidationException("Produto não encontrado.");
		}

		productTO = service.create(productTO);

		URI uri = uriBuilder.path("/products/{id}").buildAndExpand(productTO.getKey()).toUri();

		return ResponseEntity.created(uri).body(productTO);
	}

	/**
	 * {@code PUT /products/:id } : Atualização de um produto.
	 * 
	 * @param productTO o produto a ser atualizado
	 * @return O {@link ResponseEntity} com status {@code 200 (OK)} e com body a ser
	 *         atualizado {@link ProductTO}, ou com status {@code 404 (NOT FOUND)}
	 *         se o {@link ProductTO} não for encontrado.
	 */
	@Operation(summary = "Atualização de um produto")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Atualização de um produto", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ProductTO.class)) }),
			@ApiResponse(responseCode = "404", description = "Produto não encontrado.", content = @Content), })
	@PutMapping(value = "/{id}", produces = { "application/json", "application/xml", "application/x-yaml" }, consumes = {
			"application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<ProductTO> updateProducts(@PathVariable("id") String id, @RequestBody ProductTO productTO) {
		
		productTO = service.update(id, productTO);
		
		return ResponseEntity.ok().body(productTO);

	}

	/**
	 * {@code GET /products/:id } Busca de um produto por ID
	 * 
	 * @param id
	 * @return O {@link ResponseEntity} com status {@code 200 (OK) } ou com status {@code 404 (NOT FOUND)}
	 *         se o {@link ProductTO} não for encontrado.
	 */
	@Operation(summary = "Busca de um produto por ID")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Busca de um produto", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ProductTO.class)) }),
			@ApiResponse(responseCode = "404", description = "Produto não encontrado.", content = @Content), })
	@GetMapping(value = "/{id}", produces = { "application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<ProductTO> findById(@PathVariable("id") String id) {

		ProductTO productTO = service.findById(id);

		return ResponseEntity.ok().body(productTO);
	}

	/**
	 * 
	 * {@code GET /products/} Lista de produtos
	 * 
	 * @param page
	 * @param limit
	 * @param direction
	 * @return {@link List<ProductTO>}
	 */
	@Operation(summary = "Lista de produtos")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de produtos", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = List.class)) }),
			@ApiResponse(responseCode = "404", description = "Produtos não encontrados.", content = @Content), })
	@GetMapping(produces = { "application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<List<ProductTO>> findAll(@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "12") int limit,
			@RequestParam(value = "direction", defaultValue = "asc") String direction) {

		var sortDirection = "desc".equalsIgnoreCase(direction) ? Direction.DESC : Direction.ASC;

		Pageable pageable = PageRequest.of(page, limit, Sort.by(sortDirection, "id"));

		Page<ProductTO> products = service.findAll(pageable);

		return ResponseEntity.ok().body(products.getContent());
	}

	/**
	 * {@code GET /products/search?min_price=:min_price&max_price=:max_price&q=:q } Lista de produtos filtrados.
	 * 
	 * @param q
	 * @param minPrice
	 * @param maxPrice
	 * @param limit
	 * @param direction
	 * @return {@link List<ProductTO>}
	 */
	@Operation(summary = "Lista de produtos filtrados")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Lista de produtos", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = List.class)) }),
			@ApiResponse(responseCode = "404", description = "Produtos não encontrados.", content = @Content), })
	@GetMapping(value = "/search", produces = { "application/json", "application/xml", "application/x-yaml" })
	public ResponseEntity<List<ProductTO>> search(@RequestParam(value= "q", required = false) String q,
			@RequestParam(value = "min_price", required = false) Double minPrice,
			@RequestParam(value = "max_price", required = false) Double maxPrice,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "12") int limit,
			@RequestParam(value = "direction", defaultValue = "asc") String direction) {

		var sortDirection = "desc".equalsIgnoreCase(direction) ? Direction.DESC : Direction.ASC;

		Pageable pageable = PageRequest.of(page, limit, Sort.by(sortDirection, "id"));

		ProductFilter filter = new ProductFilter(q, minPrice, maxPrice);

		Page<ProductTO> products = service.search(filter, pageable);

		return ResponseEntity.ok(products.getContent());
	}

	/**
	 * {@code DELETE /products/:id } Deleção de um produto
	 * 
	 * @param id
	 * @return
	 */
	@Operation(summary = "Deleção de um produto")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Deleção de um produto.", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = List.class)) }),
			@ApiResponse(responseCode = "404", description = "Produto não encontrado.", content = @Content), })
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") String id) {

		service.delete(id);

		return ResponseEntity.ok().build();
	}

}
