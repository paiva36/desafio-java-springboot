package com.compassouol.ms.products.commons;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonPropertyOrder({ "status_code", "message"})
public class ExceptionResponse {
	
	@JsonProperty("status_code")
	private Integer statusCode;
	private String message;
	

}
