package com.compassouol.ms.products.repository.specs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.compassouol.ms.products.domain.Product;
import com.compassouol.ms.products.service.filter.ProductFilter;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 
 * @author paiva
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductSpecs {

	public static Specification<Product> withFilter(ProductFilter filter) {

		return (root, query, builder) -> {

			List<Predicate> predicates = new ArrayList<>();

			if (StringUtils.isNotBlank(filter.getQ())) {
				predicates.add(builder.or(
						builder.like(builder.lower(root.get("name")), "%" + filter.getQ().toLowerCase() + "%"),
						builder.like(builder.lower(root.get("description")), "%" + filter.getQ().toLowerCase() + "%")	
				));
			}

			if (filter.getMinPrice() != null || filter.getMaxPrice() != null) {
				
				predicates.add(builder.between(root.get("price"), filter.getMinPrice(), filter.getMaxPrice()));
			}

			return builder.and(predicates.toArray(new Predicate[0]));
		};

	}
}
