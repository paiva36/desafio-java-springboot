package com.compassouol.ms.products.service.filter;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProductFilter {

	
	private String q;
	
	@JsonProperty("min_price")
	private Double minPrice;
	
	@JsonProperty("max_price")
	private Double maxPrice;
}
