package com.compassouol.ms.products.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.compassouol.ms.products.domain.Product;
import com.compassouol.ms.products.repository.ProductRepository;
import com.compassouol.ms.products.repository.specs.ProductSpecs;
import com.compassouol.ms.products.service.converter.DozerConverter;
import com.compassouol.ms.products.service.exceptions.ProductNotFoundException;
import com.compassouol.ms.products.service.exceptions.ProductValidationException;
import com.compassouol.ms.products.service.filter.ProductFilter;
import com.compassouol.ms.products.service.to.ProductTO;

import lombok.RequiredArgsConstructor;

/**
 * 
 * @author paiva
 *
 */
@RequiredArgsConstructor
@Service
public class ProductService {
	
	private final ProductRepository repository;
	
	/**
	 * 
	 * @param pageable {@link Pageable}
	 * @return {@link Page}
	 */
	public Page<ProductTO> findAll(Pageable pageable) {
		
		var page = repository.findAll(pageable);
		
		return page.map(this::convertToProductTO);
	}
	
	/**
	 * 
	 * @param filter {@link ProductFilter}
	 * @param pageable {@link Pageable}
	 * @return {@link Page}
	 */
	public Page<ProductTO> search(ProductFilter filter, Pageable pageable) {
		
		var page = repository.findAll(ProductSpecs.withFilter(filter), pageable);
		
		return page.map(this::convertToProductTO);
	}
	
	/**
	 * 
	 * @param id 
	 * @param productTO {@link ProductTO}
	 * @return {@link ProductTO}
	 */
	public ProductTO update(String id, ProductTO productTO) {
		
		productTO.setKey(id);
		
		var entity = repository.findById(productTO.getKey())
				.orElseThrow(() -> new ProductValidationException("Produto não encontrado."));
		
		entity = DozerConverter.parseObject(productTO, Product.class);
		
		return DozerConverter.parseObject(repository.save(entity), ProductTO.class);
	}
	
	/**
	 * 
	 * @param id
	 */
	public void delete(String id) {
		
		Product entity = repository.findById(id)
				.orElseThrow(() -> new ProductNotFoundException());
		
		repository.delete(entity);
		
	}
	
	/**
	 * 
	 * @param id
	 * @return {@link ProductTO}
	 */
	public ProductTO findById(String id) {
		
		var entity = repository.findById(id)
				.orElseThrow(() -> new ProductValidationException("Produto não encontrado."));
		
		return DozerConverter.parseObject(entity, ProductTO.class);
	}
	
	/**
	 * 
	 * @param productTO {@link ProductTO}
	 * @return {@link ProductTO}
	 */
	public ProductTO create(ProductTO productTO) {
		
		var entity = DozerConverter.parseObject(productTO, Product.class);
		
		var newEntity = repository.save(entity);
		
		return DozerConverter.parseObject(newEntity, ProductTO.class);
	}
	
	/**
	 * 
	 * @param entity {@link Product}
	 * @return {@link ProductTO}
	 */
	private ProductTO convertToProductTO(Product entity) {
		
		return DozerConverter.parseObject(entity, ProductTO.class);
		
	}

}
