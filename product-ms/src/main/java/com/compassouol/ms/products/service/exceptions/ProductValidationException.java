package com.compassouol.ms.products.service.exceptions;

public class ProductValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProductValidationException(String message) {
		super(message);
	}

}
