package com.compassouol.ms.products.service.to;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.github.dozermapper.core.Mapping;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@JsonPropertyOrder({ "id", "name", "description", "price" })
public class ProductTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4846590947970228967L;
	
	
	@Mapping("id")
	@JsonProperty("id")
	private String key;
	private String name;
	private String description;
	private BigDecimal price;

}
