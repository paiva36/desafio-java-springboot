CREATE TABLE IF NOT EXISTS `product` (
  `id` varchar(500) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(80) NOT NULL,
  `price` double NOT NULL,	
  PRIMARY KEY (`id`)
)